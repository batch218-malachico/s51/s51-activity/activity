import {useState, useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function CourseCard({course}) {

	const {name, description, price, id} = course;

	// count - getter
	// setCount - setter
	// useState(0) - useState(initialGetterValue) 
	const [count, setCount] = useState(0);

	// S51 ACTIVITY
	const [seats, setSeats] = useState(5);
	// S51 Activity end

	// console.log(useState(0));

	const [isOpen, setIsOpen] = useState(true);

	// function that keeps track of the enrollees for a course
	function enroll() {
		setCount(count + 1);
		console.log('Enrollees: ' + count);

	// S51 ACTIVITY
		setSeats(seats - 1);
		console.log('Seats: ' + seats);

		// if (seats === 1) {
		// 	alert("No more seats!")
		// 	document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
		// }
	// s51 Activity END....

	}

	// useEffect allows us to execute function if the value of seats state changes.
	useEffect(() => {
		if (seats === 0) {
			setIsOpen(false);
			alert("No more seats!")
			document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
		}
		// will run anytime of the values in the array of 
	}, [seats])

	// Checks to see if the data was successfully passed
	// console.log(props);
	// Every components receives information in a form on an object
	// console.log(typeof props);

	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price</Card.Subtitle>
	        <Card.Text>PhP {price}</Card.Text>
	        <Card.Text>Enrollees:  {count}</Card.Text>
	        <Card.Text>Seats:  {seats}</Card.Text>
	        <Button id={'btn-enroll-' + id} className="bg-primary" onClick={enroll}>Enroll</Button>
	    </Card.Body>
	</Card>
	)
}

// "propTypes" - are a good way of checking data type of information between components.
CourseCard.propTypes = {
	// "shape" method is used to ckeck if prop object conforms to a specific "shape"
	course: PropTypes.shape({
		// Defined properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
	})
}